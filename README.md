# Flux + React for Hotels.com SRP Demo #

A proof of concept to demonstrate the use of Flux with React to recreate the UI of the search results page for Hotels.com.

#### Flux ####
Flux is the application architecture that Facebook uses for building client-side web applications. It complements React's composable view components by utilizing a unidirectional data flow. It's more of a pattern rather than a formal framework, and you can start using Flux immediately without a lot of new code.
https://facebook.github.io/flux/

#### React ####
React abstracts away the DOM from you, giving a simpler programming model and better performance. 
http://facebook.github.io/react/

## Problem ##
Loading the search results page takes a while to load. The generation of the HTML takes quite some time before it gets sent back to the user.

## Idea ##
* Only return the static part of the page initially, less computing on the server
* Read the JSON data from the server asynchronously
* Let the client side complete the layout with the returned data (using javascript, React for this case)
* Componentization (atomizing) of re-usable sections

Overlayed sections below will be dynamic and will only be placeholders on initial load. Explore http://cloudcannon.com/deconstructions/2014/11/15/facebook-content-placeholder-deconstruction.html

![multiple-components.png](https://bitbucket.org/repo/ML6xyd/images/2858673825-multiple-components.png)

## Process ##
![React.js-Flux-3.png](https://bitbucket.org/repo/ML6xyd/images/3156873010-React.js-Flux-3.png)

As a proof of concept, we will be using Flux with React due to its fast virtual DOM concept. This is a technology open sourced by Facebook and used in Facebook and Instagram. 

Other known alternatives are Backbone, Angular, Ember.

![flux-simple-f8-diagram-explained-1300w.png](https://bitbucket.org/repo/ML6xyd/images/3470848769-flux-simple-f8-diagram-explained-1300w.png)

## Concepts ##

### Your UI should be a function of the data ###
In many "jQuery soup" style applications, the business logic for the application, the app's data, and the UI interaction code are all intermingled. This makes these sorts of applications difficult to debug and, especially, difficult to grow. React, like many modern client-side application frameworks, enforce the idea that the UI is just a representation of your data. If you want your UI to change, you should change a piece of data and allow whatever binding system the framework uses to update the UI for you.

In React, each component is (ideally) a function of two pieces of data–the properties passed to the component instance, and the state that the component manages internally. Given the same properties (or "props") and state, the component should render in the same way.

### Don't touch the DOM ###

In React, even more so than other data-bound frameworks, you should try not to manipulate the DOM directly if at all possible. A lot of React's performance and complexity characteristics are only possible because React uses a virtual DOM with diffing algorithms internally to operate on the real DOM. Any time you build a component that reaches out and does its own DOM manipulation, you should ask yourself if you could build the same feature more idiomatically with React's virtual DOM features.

Of course, sometimes you'll need to access the DOM, or you'll want to incorporate some jQuery plugin without rebuilding it in React. For times like these, React gives you good component lifecycle hooks that you can use to ensure that React's performance doesn't suffer too much (or, in some cases, to keep your component from plain breaking).

Not manipulating the DOM goes hand-in-hand with "UI as a function of the data," above.

### Create composable components ###

A lot of times, it can be tempting to write a large component that manages several pieces of state or several pieces of UI logic. Where possible (and within reason), you should consider breaking larger components into smaller ones that operate on a single piece of data or UI logic. This makes it much easier to extend and move around pieces of your application.

## How do I get set up? ##
Clone the repo
```
git clone https://efrenmacasaet@bitbucket.org/efrenmacasaet/hotels-react.git
```

Install all the node modules

```
$ npm install
```

Start the development server. This will use browserify to build the javascript and watchify to re-build the javascript on any changes.
```
$ npm run start-dev
```

To start the server without any javascript watching
```
$ npm start
```

Point your browser to http://localhost:5000

Checkout the package.json file for the npm scripts.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Efren Macasaet (emacasaet@hotels.com)
* Other community or team contact

### Resources ###
* http://blog.andrewray.me/flux-for-stupid-people/
* http://engineering.tilt.com/roll-your-own-ab-tests-with-optimizely-and-react/