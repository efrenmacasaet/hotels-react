var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var ResultConstants = require('../constants/ResultConstants');
var assign = require('object-assign');

var CHANGE_EVENT = 'change',
    _results = [];

function setResults(results){
    _results = results
}

var SearchStore = assign({}, EventEmitter.prototype, {
    getResults: function() {
        return _results;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

// Register callback to handle all updates
SearchStore.dispatchToken = AppDispatcher.register(function(payload) {
    var action = payload.action;

    switch(action.actionType) {
        case ResultConstants.RESULT_RECEIVE:
            setResults(action.results);
            break;
        default:
            return true;
    }
    SearchStore.emitChange();
    return true;
});
module.exports = SearchStore;
