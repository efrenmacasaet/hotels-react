var React = require('react');
var ResultItem = require('./ResultItem.react');

var MainSection = React.createClass({

    render: function() {
        if (Object.keys(this.props.results).length < 1) {
            return null;
        }

        var results = this.props.results.data.body.searchResults.results;
        var hotels = [];
        for (var key in results) {
            hotels.push(<ResultItem key={key} hotel={results[key]} />);
        }

        return (
            <ol className="listings infinite-scroll-enabled">
                {hotels}
            </ol>
        );
    }
});

module.exports = MainSection;
