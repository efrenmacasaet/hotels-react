var React = require('react');

var ResultItem = React.createClass({
    propTypes: {
        hotel: React.PropTypes.object.isRequired
    },

    render: function() {
        var hotel = this.props.hotel;
        return (
            <li className="hotel" key={hotel.id}>
                <article>
                    <div className="hotel-wrap">
                        <div className="description h-card resp-module">
                        <h3 className="p-name has-welcome-rewards"><a href={hotel.urls.pdpDescription}>{hotel.name}</a></h3>
                            <p>{hotel.address.streetAddress} {hotel.address.extendedAddress} {hotel.address.locality} {hotel.address.region}</p>
                            <div className="image-and-details">
                                <div className="image">
                                    <img className="u-photo" src={hotel.thumbnailUrl} />
                                </div>
                                <div className="details resp-module">
                                    <div className="additional-details resp-module">
                                        <div className="star-rating-and-amenities resp-module">
                                            <div className="star-rating">

                                            </div>
                                        </div>
                                        <div className="location-info resp-module">
                                        <a className="map-link" href="{hotel.urls.pdpMap}">{hotel.neighbourhood}</a>
                                        </div>
                                    </div>
                                    <div className="guest-reviews resp-module">
                                        <div className="cont cont-bd cont-speech">
                                            <div className="guest-rating-badge guest-rating-{hotel.guestReviews.badge}">
                                                <div className="guest-rating-value">
                                                    {hotel.guestReviews.badgeText}
                                                    <strong>{hotel.guestReviews.rating}</strong>/{hotel.guestReviews.scale}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="pricing-bg"></div>
                        <div className="pricing resp-module">
                            <div className="price">
                                <span className="old-price-cont">
                                    <ins>{hotel.ratePlan.price.current}</ins>
                                </span>
                            </div>
                            <div className="price-breakdown">
                                <span className="price-info">
                                    {hotel.ratePlan.price.info}
                                </span>
                            </div>
                            <div className="cta-wrap">
                                <a className="cta" href={hotel.urls.pdpDescription}>
                                        Continue
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
            </li>
        )
    },

});

module.exports = ResultItem;
