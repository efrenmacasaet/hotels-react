var React = require('react');
var SearchStore = require('../stores/SearchStore');
var ActionCreator = require('../actions/ActionCreator');

var Filter = React.createClass({
    getInitialState: function() {
        return {
            results: {}
        };
    },

    componentWillMount: function () {
        SearchStore.addChangeListener(this._onChange);
    },

    componentDidMount: function() {
        ActionCreator.getResults();
    },

    componentWillUnmount: function() {
        SearchStore.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            results: SearchStore.getResults()
        });
    },
    render: function() {
        return (
            <ul>
            {
                this.props.results.data.body.filters.neighbourhood.items.map(function(filter){
                    return <li>
                        <input type="checkbox" value="{filter.value}" />
                        <label>{filter.label}</label>
                    </li>
                })
            }
            </ul>
        )
    }
});

module.exports = Filter;
