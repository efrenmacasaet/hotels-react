var MainSection = require('./MainSection.react');
var React = require('react');
var SearchStore = require('../stores/SearchStore');
var ActionCreator = require('../actions/ActionCreator');

var SearchApp = React.createClass({

    getInitialState: function() {
        return {
            results: {}
        };
    },

    componentWillMount: function () {
        SearchStore.addChangeListener(this._onChange);
    },

    componentDidMount: function() {
        ActionCreator.getResults();
    },

    componentWillUnmount: function() {
        SearchStore.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            results: SearchStore.getResults()
        });
    },

    render: function() {
        var results;
        return (
            <MainSection results={this.state.results} />
        )
    },
});

module.exports = SearchApp;
