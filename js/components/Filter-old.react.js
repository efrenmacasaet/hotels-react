var MainSection = require('./MainSection.react');
var React = require('react');
var SearchStore = require('../stores/SearchStore');

var Filter = React.createClass({

    getInitialState: function() {
        return {};
    },

    componentDidMount: function() {
        //ResultStore.addChangeListener(this._onChange);
        this.getSearchState();
    },

    componentWillUnmount: function() {
        ResultStore.removeChangeListener(this._onChange);
    },

    render: function() {
        if (this.state.response) {
            console.log(this.state.response);
            return (
                <ul>
                {
                    this.state.response.data.body.filters.neighbourhood.items.map(function(filter){
                        return <li>
                            <input type="checkbox" value="{filter.value}" />
                            <label>{filter.label}</label>
                        </li>
                    })
                }
                </ul>
            )
        } else {
            return null;
        }
    },

    _onChange: function() {
        this.setState(getSearchState());
    }

});

module.exports = Filter;
