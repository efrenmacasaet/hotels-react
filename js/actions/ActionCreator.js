var Dispatcher = require('../dispatcher/AppDispatcher');
var ResultConstants = require('../constants/ResultConstants');
var Store = require('../stores/SearchStore');
var Promise = require('es6-promise').Promise; // jshint ignore:line
var Api = require('../services/Api');


var ActionCreator = {
    getResults: function () {
        Api
            .get('/results.json')
            .then(function(results){
                Dispatcher.handleViewAction({
                    actionType: ResultConstants.RESULT_RECEIVE,
                    results: results
                });
            })
            .catch(function(){
                Dispatcher.handleViewAction({
                    actionType: ResultConstants.RESULT_ERROR,
                    error: "Error"
                });
            });
    }
}

module.exports = ActionCreator;
