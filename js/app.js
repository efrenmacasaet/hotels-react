var React = require('react');
var SearchApp = require('./components/SearchApp.react');
var Filter = require('./components/Filter.react');

React.render(
  <SearchApp />,
  document.getElementById('listings')
);
React.render(
  <Filter />,
  document.getElementById('filter-neighbourhood')
);

