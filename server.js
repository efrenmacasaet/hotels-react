var http = require('http');
var router = require('routes')();
var ecstatic = require('ecstatic')(__dirname + '/');

var server = http.createServer(function (req, res) {
    var m = router.match(req.url);
    if (m) m.fn(req, res, m.params);
    else ecstatic(req, res);
});

server.listen(5000, function () {
    console.log('listening on :' + server.address().port);
});

